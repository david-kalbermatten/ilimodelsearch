# IliModelSearch - backend <!-- omit in toc -->
This ReadMe contains information about the backend component of IliModelsearch

- [REST API](#rest-api)
  - [Search](#search)
  - [IliFile](#ilifile)
  - [Harvester](#harvester)
- [How to contribute and run the project locally](#how-to-contribute-and-run-the-project-locally)
  - [Requirements](#requirements)
  - [URLs](#urls)
  - [Setup](#setup)
    - [Core](#core)
    - [ElasticSearch](#elasticsearch)
    - [Harvester](#harvester-1)
    - [Backend](#backend)

# REST API
## Search
The rest api can be accessed by sending a `GET`-Request to the following path: 

**GET** `<HOST/IP>:8080/search/{your search term}`

This request will return the following `JSON`-Output in its response body:

```json
[
  {
    "id": 1,
    "url": "http://models.interlis.ch/obsolete/IliRepository09_o0.ili",
    "name": "IliFile 1",
    "schemaLanguage": "ili2_3",
    "content": "This string contains the entire iliFile-Content",
    "version": "2021-12-21",
    "md5": "693d9a0698aff95c",
    "invalidEncoding": false
  },
]
```

## IliFile
**GET/DELETE/PUT/POST** `<HOST/IP>:8080/ilifiles/{id}`

_`POST` mapping doesn't require an id_
```yaml
{
  "id": 1,
  "url": "http://models.interlis.ch/obsolete/IliRepository09_o0.ili",
  "name": "IliFile 1",
  "schemaLanguage": "ili2_3",
  "content": "This string contains the entire iliFile-Content",
  "version": "2021-12-21",
  "md5": "693d9a0698aff95c",
  "invalidEncoding": false
}
```

**GET** `<HOST/IP>:8080/ilifiles/count`

Running this GET-Request will return the number of entries in the Database:
```html
1788
```

## Harvester
**GET** `<HOST/IP>:8080/harvester/run`

Running this GET-Request will either start the harvester and/or return the status:

_Status: started_
```json
started
```

_Status: running_
```json
running
```

# How to contribute and run the project locally
This paragraph describes hot to setup the development environment(s).

## Requirements
- Java JDK Version 11
- IntelliJ
- Git
- Docker environment (Docker Desktop on Windows)
- Webbrowser or Postman to query search requests

## URLs
| Component           | URL                                          | Description                                                   |
|---------------------|----------------------------------------------|---------------------------------------------------------------|
| Kibana Dev Console  | http://localhost:5601/app/dev_tools#/console | The development stack includes Kibana for debugging purposes  |
| Search API          | http://localhost:8080/search/{search_term}   | replace `{search_term}` with the string to used to search     |
| IliFile API         | http://localhost:8080/ilifiles/              | this path is used to ingest and get specific IliFiles         |
| IliFile API (Count) | http://localhost:8080/ilifiles/count         | this path is used to retriev the number of IliFiles stored    |
| Harvester API       | http://localhost:8080/harvester/run          | this path is used to run the harvester portion of the backend |

## Setup
In order for the seperate projects to work you need to use `maven install` to make the maven projects locally available. __The order of installation matters!__

`git clone https://gitlab.com/geometalab/ilimodelsearch.git`

### Core
1. Open the `core` folder as a project in IntelliJ
2. Wait for IntelliJ to resolve all dependencies.
3. Open the maven side menu in IntelliJ and run the following commands by clicking them
   1. `clean`
   2. `install`

After sucessfully running this you should be able to use the `core`-package in the other two projects: `harvester`, `backend`

### ElasticSearch
1. Make sure Docker Desktop is running
2. Use the following command to launch the ElasticSearch Development Server
```bash
docker-compose up "./backend/docker-compose.yml"
```

### Harvester
1. Open the `harvester` folder as a project in IntelliJ
2. Wait for IntelliJ to resolve all dependencies.
3. Open the maven side menu in IntelliJ and run the following commands by clicking them
   1. `clean`
   2. `install`

The harvester is setup to compare with the existing data set and update where needed.

### Backend
_The backend uses the H2-Memory DB._

1. Open the `backend` folder as a project in IntelliJ
2. Wait for IntelliJ to resolve all dependencies.
3. Navigate to the class called `IlimodelSearchApplication`
4. Press the green launch button to run the backend-server

At this point in time there is not data since the `harvester` component hasn't been run yet.

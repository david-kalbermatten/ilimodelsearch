package ch.ost.rj.ifs.ilimodelsearch.backend;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.Socket;

@Slf4j
@Configuration
public class ElasticsearchConfig {

    @Value("${elasticsearch.host}")
    private String elasticsearchHost;

    @Value("${elasticsearch.port}")
    private int elasticsearchPort;

    @Bean(destroyMethod = "close")
    public RestHighLevelClient client() {
        if(isRunning()) {
            return new RestHighLevelClient((
                    RestClient.builder(new HttpHost(elasticsearchHost, elasticsearchPort))
            ));
        } else {
            log.error("ElasticSearch server is not running or not configured correctly!");
            System.exit(-1);
        }
        return null;
    }

    private boolean isRunning() {
        try (Socket s = new Socket(elasticsearchHost, elasticsearchPort)) {
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}

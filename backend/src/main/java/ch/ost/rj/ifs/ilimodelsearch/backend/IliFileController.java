package ch.ost.rj.ifs.ilimodelsearch.backend;

import ch.ost.rj.ifs.ilimodelsearch.backend.exceptions.IliFileNotFoundException;
import ch.ost.rj.ifs.ilimodelsearch.backend.repository.IliFileRepository;
import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import ch.ost.rj.ifs.ilimodelsearch.harvester.repository.Repository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class IliFileController implements Repository {
    private final IliFileRepository repository;
    private final ProfileService service;
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Value("${backend.resetCode}")
    private String resetCode;

    @GetMapping("/search/{term}")
    List<IliFile> getSearchIndexes(@PathVariable String term) {

        List<String> ids = new ArrayList<>();
        try {
            ids = service.search(term);
        } catch (IOException e) {
            log.error("Something went wrong when accessing Search API");
            e.printStackTrace();
        }
        List<IliFile> iliFiles = new ArrayList<>();
        ids.forEach(idString -> iliFiles.add(one(idString)));
        return iliFiles;
    }

    @GetMapping("/ilifiles")
    List<IliFile> all() {
        return repository.findAll();
    }

    @PostMapping("/ilifiles")
    ResponseEntity<IliFile> newIliFile(@RequestBody IliFile newIlifile) {
        IliFile iliFileFromDB = repository.save(newIlifile);
        log.info("Added new iliFile: " + iliFileFromDB);
        try {
            service.createEsEntry(iliFileFromDB);
        } catch (IOException e) {
            log.error("Calling Elastic Search Service raised an Error!", e);
        }
        return new ResponseEntity<>(iliFileFromDB, HttpStatus.CREATED);
    }

    @GetMapping("/ilifiles/{id}")
    IliFile one(@PathVariable String id) {
        return repository.findById(id)
                .orElseThrow(() -> new IliFileNotFoundException(id));
    }

    @GetMapping("/ilifiles/count")
    long getNumberOfEntries() {
        return repository.count();
    }

    @PutMapping("/ilifiles/{id}")
    IliFile replaceIliFile(@RequestBody IliFile newIliFile, @PathVariable String id) {
        IliFile iliFile = repository.findById(id)
                .map(iliFileFromDB -> {
                    iliFileFromDB.setUrl(newIliFile.getUrl());
                    iliFileFromDB.setName(newIliFile.getName());
                    iliFileFromDB.setSchemaLanguage(newIliFile.getSchemaLanguage());
                    iliFileFromDB.setContent(newIliFile.getContent());
                    iliFileFromDB.setVersion(newIliFile.getVersion());
                    iliFileFromDB.setMd5(newIliFile.getMd5());
                    iliFileFromDB.setValidEncoding(newIliFile.isValidEncoding());
                    return repository.save(iliFileFromDB);
                })
                .orElseGet(() -> {
                    newIliFile.setId(id);
                    return repository.save(newIliFile);
                });
        try {
            service.createEsEntry(iliFile);
        } catch (IOException e) {
            log.error("Calling Elastic Search Service raised an Error!", e);
        }
        log.info("Added new iliFile: " + iliFile);
        return iliFile;
    }

    @DeleteMapping("/ilifiles/{id}")
    void deleteIliFile(@PathVariable String id) {
        repository.deleteById(id);
    }

    @GetMapping("/reset/{passcode}")
    String resetBackendData(@PathVariable String passcode) throws IOException {
        if(passcode.equals(resetCode)) {
            service.resetIndex();
            repository.deleteAll();
            return "All Items deleted!";
        }

        return "Wrong passcode! Nothing has been deleted...";
    }

    @Override
    public IliFile findById(String s) {
        if (s == null) {
            s = "";
        }
        return one(s);
    }

    @Override
    public void put(IliFile iliFile) {
        replaceIliFile(iliFile, iliFile.getId());
    }
}

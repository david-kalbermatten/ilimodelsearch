package ch.ost.rj.ifs.ilimodelsearch.backend;

import ch.ost.rj.ifs.ilimodelsearch.backend.helper.TerminateBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@Slf4j
@SpringBootApplication
@EntityScan(basePackages = "ch.ost.rj.ifs.ilimodelsearch.core.model")
@EnableJpaRepositories(basePackages = "ch.ost.rj.ifs.ilimodelsearch.backend")
@EnableAsync
public class IlimodelsearchApplication {
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(IlimodelsearchApplication.class, args);
		ctx.getBean(TerminateBean.class);

	}
}

package ch.ost.rj.ifs.ilimodelsearch.backend;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.*;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class ProfileService {
    private RestHighLevelClient client;

    @Autowired
    public ProfileService(RestHighLevelClient client) {
        this.client = client;
    }

    public String createEsEntry(IliFile iliFile) throws IOException {
        IndexRequest indexRequest = new IndexRequest("ilifile");
        indexRequest.id(iliFile.getId());
        indexRequest.source(iliFile.toJson(), XContentType.JSON);

        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);

        return indexResponse.getResult().toString();
    }

    public List<String> search(String searchTerm) throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("content", searchTerm);
        matchQueryBuilder.fuzziness(Fuzziness.fromEdits(2));
        matchQueryBuilder.boost(20);
        searchSourceBuilder.query(matchQueryBuilder);
        searchSourceBuilder.fetchSource(false);
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices("ilifile");

        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits searchHits = response.getHits();
        List<SearchHit> searchHitList = Arrays.asList(searchHits.getHits());

        List<String> indexes = new ArrayList<>();
        searchHitList.forEach(searchHit -> indexes.add(searchHit.getId()));

        return indexes;
    }

    public void setupPutMapping() {
        boolean exists = false;
        try {
            GetIndexRequest getIndexRequest = new GetIndexRequest("ilifile");
            exists = client.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("Couldn't connect to ElasticSearch while checking index!", e);
        }

        if (!exists) {
            log.info("Creating Mapping for ilifile");
            CreateIndexRequest request = new CreateIndexRequest("ilifile");
            request.settings(Settings.builder()
                    .put("index.number_of_shards", 3)
                    .put("index.number_of_replicas", 2)
            );

            try {
                XContentBuilder builder = XContentFactory.jsonBuilder();
                builder.startObject().startObject("properties").
                        startObject("url").field("type", "text").endObject().
                        startObject("name").field("type", "text").endObject().
                        startObject("schemaLanguage").field("type", "text").endObject().
                        startObject("content").field("type", "text").endObject().
                        startObject("version").field("type", "text").endObject().
                        startObject("md5").field("type", "text").endObject().
                        startObject("validEncoding").field("type", "boolean").endObject().
                        endObject().endObject();
                request.mapping(builder);
            } catch (IOException e) {
                log.error("Couldn't setup builder for mapping correctly!", e);
            }

            try {
                CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
                log.info("Index creation was successful: " + createIndexResponse.isAcknowledged());
            } catch (IOException e) {
                log.error("Index creating was unsuccessful", e);
            }
        } else {
            log.info("Index is already setup");
        }

    }

    public void resetIndex() throws IOException {
        DeleteByQueryRequest request = new DeleteByQueryRequest("ilifile");
        request.setConflicts("proceed");
        request.setQuery(QueryBuilders.matchAllQuery());
        client.deleteByQuery(request, RequestOptions.DEFAULT);
    }
}
package ch.ost.rj.ifs.ilimodelsearch.backend.configuration;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public abstract class ExampleData {
    public static List<IliFile> generateExampleIliFileList() throws MalformedURLException {
        List<IliFile> iliFiles = new ArrayList<>();
        iliFiles.add(new IliFile(
                new URL("https://models.geo.ar.ch/ARE/AR_Schutzplanung_kommunal_20191101.ili"),
                "AI_Kirchgemeinden",
                "ili2_3",
                "INTERLIS 2.3;\n" +
                        "\n" +
                        "/** Geodatenmodell Kirchgemeinden Appenzell I.Rh. AI\n" +
                        " *\n" +
                        " */\n" +
                        "\n" +
                        "!! Date       | Version | Who           | Modification\n" +
                        "!!------------------------------------------------------------------------------\n" +
                        "!! 2020-08-17 | 1.0     | VMA AI        | Modell Erstellung\n" +
                        "\n" +
                        "!!@ technicalContact=mailto:geoinformation@ai.ch\n" +
                        "!!@ furtherInformation=https://geo.ai.ch\n" +
                        "!!@\n" +
                        "\n" +
                        "!!@ technicalContact=mailto:geoinformation@ai.ch\n" +
                        "!!@ furtherInformation=https://geo.ai.ch\n" +
                        "!!@\n" +
                        "\n" +
                        "MODEL AI_Kirchgemeinden (de)\n" +
                        "  AT \"https://models.geo.ai.ch/VA/\"\n" +
                        "  VERSION \"2020-08-17\" =\n" +
                        "\n" +
                        "  IMPORTS UNQUALIFIED INTERLIS;\n" +
                        "  IMPORTS Units;\n" +
                        "  IMPORTS GraphicCHLV95_V1;\n" +
                        "  IMPORTS GeometryCHLV95_V1;\n" +
                        "  IMPORTS CHAdminCodes_V1;\n" +
                        "  IMPORTS InternationalCodes_V1;\n" +
                        "  IMPORTS LocalisationCH_V1;\n" +
                        "\n" +
                        "  DOMAIN\n" +
                        "\n" +
                        "    INTEGER8 = 0 .. 99999999;\n" +
                        "    REAL84 = 0.0000 .. 99999999.9999;\n" +
                        "\n" +
                        "    Punkt =\n" +
                        "      COORD 2741300.000 .. 2764500.000 [m]\n" +
                        "           ,1233200.000 .. 1257100.000 [m]\n" +
                        "           ,ROTATION 2 -> 1;\n" +
                        "\n" +
                        "    Linie =\n" +
                        "      POLYLINE WITH (ARCS,STRAIGHTS) VERTEX Punkt;\n" +
                        "\n" +
                        "    Einzelflaeche =\n" +
                        "      SURFACE WITH (ARCS,STRAIGHTS) VERTEX Punkt WITHOUT OVERLAPS > 0.05;\n" +
                        "\n" +
                        "    Gebietseinteilung =\n" +
                        "      AREA WITH (ARCS,STRAIGHTS) VERTEX Punkt WITHOUT OVERLAPS > 0.05;\n" +
                        "\n" +
                        "    Rechtsstatus = (\n" +
                        "      projektiert (\n" +
                        "        Entwurf,\n" +
                        "        im_Einsprache_Auflageverfahren,\n" +
                        "        im_Genehmigungsverfahren,\n" +
                        "        im_Rechtsmittelverfahren\n" +
                        "        ),\n" +
                        "      rechtskraeftig (\n" +
                        "        in_Kraft,\n" +
                        "        Aufhebung_Entwurf,\n" +
                        "        Aufhebung_im_Einsprache_Auflageverfahren,\n" +
                        "        Aufhebung_im_Genehmigungsverfahren,\n" +
                        "        Aufhebung_im_Rechtsmittelverfahren\n" +
                        "        ),\n" +
                        "      aufgehoben\n" +
                        "      );\n" +
                        "\n" +
                        "    Verbindlichkeit = (\n" +
                        "      Nutzungsplanfestlegung,\n" +
                        "      orientierend,\n" +
                        "      hinweisend,\n" +
                        "      wegleitend\n" +
                        "    );\n" +
                        "\n" +
                        "    TextSize = (\n" +
                        "      klein,\n" +
                        "      mittel,\n" +
                        "      gross,\n" +
                        "      unterdrueckt\n" +
                        "    );\n" +
                        "\n" +
                        "\n" +
                        "  TOPIC Transfermetadaten =\n" +
                        "    OID AS INTERLIS.UUIDOID;\n" +
                        "\n" +
                        "    CLASS Stelle =\n" +
                        "      Name : MANDATORY TEXT*80;\n" +
                        "      Stelle_im_Web : URI;\n" +
                        "      UID : TEXT*12;\n" +
                        "    END Stelle;\n" +
                        "\n" +
                        "    CLASS Datenbestand =\n" +
                        "      Gemeinde : 3101..3111;\n" +
                        "      Gegenstand : MANDATORY TEXT*255;\n" +
                        "      Stand : MANDATORY INTERLIS.XMLDate;\n" +
                        "      Lieferdatum : MANDATORY INTERLIS.XMLDate;\n" +
                        "      Bemerkungen : MTEXT;\n" +
                        "    END Datenbestand;\n" +
                        "\n" +
                        "    ASSOCIATION zustStelle_DatenAssoc =\n" +
                        "      DatenbestandRef -- {0..*} Datenbestand;\n" +
                        "      zustaendigeStelleRef -<> {1} Stelle;\n" +
                        "    END zustStelle_DatenAssoc;\n" +
                        "\n" +
                        "  END Transfermetadaten;\n" +
                        "\n" +
                        "\n" +
                        "  TOPIC Kirchgemeinden =\n" +
                        "    OID AS INTERLIS.UUIDOID;\n" +
                        "\n" +
                        "    CLASS ZustaendigkeitKataster =\n" +
                        "      Zustaendige_Behoerde : MANDATORY LocalisationCH_V1.MultilingualText;\n" +
                        "      URL_Behoerde : MANDATORY INTERLIS.URI;\n" +
                        "      UID : TEXT*12;\n" +
                        "      Katastername : MANDATORY LocalisationCH_V1.MultilingualText;\n" +
                        "      URL_Kataster : MANDATORY INTERLIS.URI;\n" +
                        "    END ZustaendigkeitKataster;\n" +
                        "\n" +
                        "    CLASS Kirchgemeinde =\n" +
                        "      Geometry : GeometryCHLV95_V1.MultiSurface;\n" +
                        "      ID : MANDATORY INTEGER8;\n" +
                        "      Info : TEXT*80;\n" +
                        "      Flaeche : REAL84;\n" +
                        "      Rechtsstatus : MANDATORY Rechtsstatus;\n" +
                        "      Verbindlichkeit : MANDATORY Verbindlichkeit;\n" +
                        "      Konfession : MANDATORY (Katholisch, Evangelisch_Reformiert);\n" +
                        "      Datum_letzte_Bearbeitung : INTERLIS.XMLDate;\n" +
                        "      Datum_Entwurf : INTERLIS.XMLDate;\n" +
                        "      Datum_Verfahrenseinleitung : INTERLIS.XMLDate;\n" +
                        "      Datum_Auflage : INTERLIS.XMLDate;\n" +
                        "      Datum_Erlass : INTERLIS.XMLDate;\n" +
                        "      Datum_Genehmigung : INTERLIS.XMLDate;\n" +
                        "      Datum_Rechtskraft : INTERLIS.XMLDate;\n" +
                        "      Datum_Aufhebung : INTERLIS.XMLDate;\n" +
                        "      Bemerkungen : MTEXT;\n" +
                        "      !! Konsistenzbedingung: Rechtskraeftige Daten muessen ein Datum_Rechtskraft abgefuellt haben\n" +
                        "      MANDATORY CONSTRAINT Rechtsstatus != #rechtskraeftig.in_Kraft OR DEFINED(Datum_Rechtskraft);\n" +
                        "      MANDATORY CONSTRAINT Rechtsstatus != #rechtskraeftig.Aufhebung_Entwurf OR DEFINED(Datum_Rechtskraft);\n" +
                        "      MANDATORY CONSTRAINT Rechtsstatus != #rechtskraeftig.Aufhebung_im_Einsprache_Auflageverfahren OR DEFINED(Datum_Rechtskraft);\n" +
                        "      MANDATORY CONSTRAINT Rechtsstatus != #rechtskraeftig.Aufhebung_im_Genehmigungsverfahren OR DEFINED(Datum_Rechtskraft);\n" +
                        "      MANDATORY CONSTRAINT Rechtsstatus != #rechtskraeftig.Aufhebung_im_Rechtsmittelverfahren OR DEFINED(Datum_Rechtskraft);\n" +
                        "    END Kirchgemeinde;\n" +
                        "\n" +
                        "    ASSOCIATION ZustaendigkeitKatasterKirchgemeinde =\n" +
                        "      ZustaendigkeitKataster -- {1} ZustaendigkeitKataster;\n" +
                        "      Kirchgemeinde -<> {0..*} Kirchgemeinde;\n" +
                        "    END ZustaendigkeitKatasterKirchgemeinde;\n" +
                        "\n" +
                        "  END Kirchgemeinden;\n" +
                        "\n" +
                        "\n" +
                        "END AI_Kirchgemeinden.\n" +
                        "\n",
                "2020-08-17",
                "e0322324257c4501989a3908cf9cee38",
                true
        ));
        iliFiles.add(new IliFile(
                new URL("http://models.geo.ai.ch/AFU/AI_Laermkataster_V1_0.ili"),
                "AR_Schutzplanung_kommunal_20191101",
                "ili2_3",
                "INTERLIS 2.3;\n" +
                        "\n" +
                        "!!============================================================\n" +
                        "!!@ File                = \"AR_Schutzplanung_kommunal_20191101.ili\";\n" +
                        "!!@ Title               = \"Kommunaler Schutzzonenplan\";\n" +
                        "!!@ shortDescription    = \"Das Modell umfasst die kommunalen Schutzobjekte in Natur, Landschaft und Ortsbild.\";\n" +
                        "!!@ technicalContact    = \"mailto:gisfachstelle@ar.ch\";\n" +
                        "!!@ furtherInformation  = \"https://www.ar.ch/verwaltung/departement-bau-und-volkswirtschaft/amt-fuer-raum-und-wald/geoinformation-und-vermessung/\";\n" +
                        "!!@ zustaendige_Stelle  = \"ARW\";\n" +
                        "!!@ GeoIV_ID            = \"73\";\n" +
                        "!!@ kGeoIV_ID           = \"38-AR, 40-AR\";\n" +
                        "!!@ Compiler-Version    = \"5.0.2-20190906\"; \n" +
                        "!!------------------------------------------------------------\n" +
                        "!! \n" +
                        "!! Version\t    | Wer       | �nderung\n" +
                        "!!------------------------------------------------------------\n" +
                        "!! 2019-11-01   | GIS-FS    | Modell-Erstellung inkl. Linien\n" +
                        "!!============================================================\n" +
                        "\n" +
                        "MODEL AR_Schutzplanung_kommunal_20191101(de)\n" +
                        "  AT \"https://models.geo.ar.ch\"\n" +
                        "  VERSION \"2019-11-01\" =\n" +
                        "\n" +
                        "\n" +
                        "  IMPORTS UNQUALIFIED INTERLIS;     \n" +
                        "  IMPORTS Units;\n" +
                        "  IMPORTS GraphicCHLV95_V1;\n" +
                        "  IMPORTS GeometryCHLV95_V1;\n" +
                        "  IMPORTS AR_Raumplanung_Codeliste_20191101;\n" +
                        "\n" +
                        "  \n" +
                        "  DOMAIN\n" +
                        "    TextSize = (\n" +
                        "      klein,\n" +
                        "      mittel,\n" +
                        "      gross);\n" +
                        "\n" +
                        "    Verbindlichkeit = (\n" +
                        "      Nutzungsplanfestlegung,\n" +
                        "      orientierend,\n" +
                        "      hinweisend,\n" +
                        "      wegleitend);\n" +
                        "      \n" +
                        "    Rechtsstatus = (\n" +
                        "        in_Kraft,\n" +
                        "        laufende_Aenderung,\n" +
                        "\t\tausser_Kraft);\n" +
                        "\n" +
                        "    Verfahren_komm = (\n" +
                        "      Zonenplan,\n" +
                        "      Sondernutzungsplan,\n" +
                        "      kommunale_Schutzverordnung,\n" +
                        "      Einzelverfuegung,\n" +
                        "      Vereinbarung);\n" +
                        "\n" +
                        "\t  \n" +
                        "!!------------------------------------------------------------\n" +
                        "  TOPIC Transfermetadaten =\n" +
                        "  OID AS INTERLIS.UUIDOID;\n" +
                        "!!------------------------------------------------------------\n" +
                        "    CLASS Stelle =\n" +
                        "      Name          : MANDATORY TEXT*80;\n" +
                        "      Stelle_im_Web :           URI;\n" +
                        "    END Stelle;\n" +
                        "\n" +
                        "    CLASS Datenbestand =\n" +
                        "      BasketID      : MANDATORY TEXT*50;\n" +
                        "      Gemeinde      : MANDATORY 3001..3038;\n" +
                        "      Stand         : MANDATORY INTERLIS.XMLDate;\n" +
                        "      Lieferdatum   : MANDATORY INTERLIS.XMLDate;\n" +
                        "      Bemerkungen   :           MTEXT;\n" +
                        "    END Datenbestand;\n" +
                        "\n" +
                        "    ASSOCIATION zustStelle_DatenAssoc =\n" +
                        "      DatenbestandRef        --  {0..*} Datenbestand;\n" +
                        "      zustaendigeStelleRef   -<> {1}    Stelle;\n" +
                        "    END zustStelle_DatenAssoc;\n" +
                        "\n" +
                        "  END Transfermetadaten;\n" +
                        "\n" +
                        "\n" +
                        "!!------------------------------------------------------------\n" +
                        "  TOPIC Rechtsvorschriften =\n" +
                        "  OID AS INTERLIS.UUIDOID;\n" +
                        "!!------------------------------------------------------------\n" +
                        "    CLASS Dokument =\n" +
                        "      Titel          : MANDATORY TEXT*80;\n" +
                        "      Gemeinde       :           3001..3038;\n" +
                        "      Oereb_Doklink  :           URI;         !!Geolink OEREBlex\n" +
                        "      Geschaeft_Nr   :           TEXT*20;     !!Axioma_Nr.\n" +
                        "      Bemerkungen    :           MTEXT;\n" +
                        "    END Dokument;\n" +
                        "\n" +
                        "    CLASS Rechtsvorschrift\n" +
                        "    EXTENDS Dokument =\n" +
                        "    END Rechtsvorschrift;\n" +
                        "\n" +
                        "    ASSOCIATION HinweisWeitereDokumenteAssoc =\n" +
                        "      UrsprungRef   -- {0..*} Dokument;\n" +
                        "      HinweisRef    -- {0..*} Dokument;\n" +
                        "    END HinweisWeitereDokumenteAssoc;\n" +
                        "\n" +
                        "  END Rechtsvorschriften;\n" +
                        "\n" +
                        "\n" +
                        "!!------------------------------------------------------------\n" +
                        "  TOPIC Gde_Codelisten =\n" +
                        "  OID AS INTERLIS.UUIDOID;\n" +
                        "    DEPENDS ON\n" +
                        "      AR_Raumplanung_Codeliste_20191101.Kt_Codelisten; \n" +
                        "!!------------------------------------------------------------\n" +
                        "    CLASS Gde_Code =\n" +
                        "      Gde_Code             : MANDATORY 11000 .. 99999;\n" +
                        "      Gde_Kuerzel          : MANDATORY TEXT*12;\n" +
                        "      Gde_Bezeichnung      : MANDATORY TEXT*80;\n" +
                        "      Gde_Planbeschriftung :           TEXT*12;\n" +
                        "      Bemerkungen          :           MTEXT;\n" +
                        "    END Gde_Code;\n" +
                        "\n" +
                        "    ASSOCIATION Gde_Code_Kt_CodeAssoc =\n" +
                        "      Gde_CodeRef            --  {0..*} Gde_Code;\n" +
                        "      Kt_CodeRef (EXTERNAL)  -<> {1}    AR_Raumplanung_Codeliste_20191101.Kt_Codelisten.Kt_Code;\n" +
                        "    END Gde_Code_Kt_CodeAssoc;\n" +
                        "\n" +
                        "  END Gde_Codelisten;\n" +
                        "\n" +
                        "\n" +
                        "!!------------------------------------------------------------\n" +
                        "  TOPIC Kommunale_Schutzplanung =\n" +
                        "  OID AS INTERLIS.UUIDOID;\n" +
                        "    DEPENDS ON\n" +
                        "      AR_Schutzplanung_kommunal_20191101.Gde_Codelisten,\n" +
                        "      AR_Schutzplanung_kommunal_20191101.Rechtsvorschriften;\n" +
                        "!!------------------------------------------------------------\n" +
                        "    CLASS Kommunaler_Schutzzonenplan =\n" +
                        "      Objektnummer      :           TEXT*25;  \n" +
                        "      BFS_Nr            : MANDATORY 3001 .. 3038;\n" +
                        "      Verfahren_komm    : MANDATORY Verfahren_komm;\n" +
                        "      Verbindlichkeit   : MANDATORY Verbindlichkeit;\n" +
                        "      Rechtsstatus      : MANDATORY Rechtsstatus;\n" +
                        "      Datum_Genehmigung :           INTERLIS.XMLDate;\n" +
                        "      Datum_Rechtskraft :           INTERLIS.XMLDate;\n" +
                        "      Datum_Aufhebung   :           INTERLIS.XMLDate;   \n" +
                        "      Bemerkungen       :           MTEXT;\n" +
                        "    !! Konsistenzbedingung: Rechtskraeftige Daten muessen ein Datum_Rechtskraft abgefuellt haben\n" +
                        "      MANDATORY CONSTRAINT Rechtsstatus != #in_Kraft OR DEFINED(Datum_Rechtskraft);\n" +
                        "    END Kommunaler_Schutzzonenplan;\n" +
                        "\n" +
                        "\n" +
                        "    ASSOCIATION Kommunale_Schutzplanung_ArtAssoc =\n" +
                        "      Kommunale_SchutzplanungRef   -<> {0..*} Kommunaler_Schutzzonenplan;\n" +
                        "      Gde_CodeRef (EXTERNAL)       --  {1}    AR_Schutzplanung_kommunal_20191101.Gde_Codelisten.Gde_Code;\n" +
                        "    END Kommunale_Schutzplanung_ArtAssoc;\n" +
                        "\n" +
                        "    ASSOCIATION Kommunale_Schutzplanung_VorschriftAssoc =\n" +
                        "      Kommunale_SchutzplanungRef   -- {0..*}  Kommunaler_Schutzzonenplan;\n" +
                        "      VorschriftRef (EXTERNAL)     -- {0..*}  AR_Schutzplanung_kommunal_20191101.Rechtsvorschriften.Dokument;\n" +
                        "    END Kommunale_Schutzplanung_VorschriftAssoc;\n" +
                        "\n" +
                        "\n" +
                        "    CLASS Kommunale_Schutzplanung_Flaeche =\n" +
                        "         Geometrie : MANDATORY SURFACE WITH (STRAIGHTS, ARCS)  VERTEX GeometryCHLV95_V1.Coord2\n" +
                        "         \t     WITHOUT OVERLAPS > 0.050;\n" +
                        "    END Kommunale_Schutzplanung_Flaeche;\n" +
                        "\n" +
                        "    ASSOCIATION Kommunale_Schutzplanung_FlaecheEntstehungAssoc =\n" +
                        "      Kommunale_Schutzplanung_FlaecheRef  -- {0..*} Kommunale_Schutzplanung_Flaeche;\n" +
                        "      KommSchutzzonenplanRef              -- {1}    Kommunaler_Schutzzonenplan;\n" +
                        "    END Kommunale_Schutzplanung_FlaecheEntstehungAssoc;\n" +
                        "\n" +
                        "\t\n" +
                        "    CLASS Kommunale_Schutzplanung_Linie =\n" +
                        "      Geometrie : MANDATORY POLYLINE WITH (STRAIGHTS, ARCS) VERTEX GeometryCHLV95_V1.Coord2;\n" +
                        "    END Kommunale_Schutzplanung_Linie;\n" +
                        "\n" +
                        "    ASSOCIATION Kommunale_Schutzplanung_LinieEntstehungAssoc =\n" +
                        "      Kommunale_Schutzplanung_LinieRef  -- {0..*} Kommunale_Schutzplanung_Linie;\n" +
                        "      KommSchutzzonenplanRef            -- {1}    Kommunaler_Schutzzonenplan;\n" +
                        "    END Kommunale_Schutzplanung_LinieEntstehungAssoc;\n" +
                        "\n" +
                        "\t\n" +
                        "    CLASS Kommunale_Schutzplanung_Punktsymb =\n" +
                        "        Geometrie : MANDATORY GeometryCHLV95_V1.Coord2;\n" +
                        "    END Kommunale_Schutzplanung_Punktsymb;\n" +
                        "\n" +
                        "    ASSOCIATION Kommunale_Schutzplanung_PunktsymbEntstehungAssoc =\n" +
                        "      Kommunale_Schutzplanung_PunktsymbRef  -- {0..*} Kommunale_Schutzplanung_Punktsymb;\n" +
                        "      KommSchutzzonenplanRef                -- {1}    Kommunaler_Schutzzonenplan;\n" +
                        "    END Kommunale_Schutzplanung_PunktsymbEntstehungAssoc;\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "    CLASS Beschriftung_Kommunale_Schutzplanung \n" +
                        "\tEXTENDS GraphicCHLV95_V1.TextGraphic =\n" +
                        "      TextSize : MANDATORY TextSize;\n" +
                        "    END Beschriftung_Kommunale_Schutzplanung;\n" +
                        "\n" +
                        "    ASSOCIATION Beschriftung_Kommunale_SchutzplanungAssocAssoc =\n" +
                        "      Beschriftung_Kommunale_SchutzplanungRef  -- {0..*} Beschriftung_Kommunale_Schutzplanung;\n" +
                        "      KommSchutzzonenplanRef                   -- {1}    Kommunaler_Schutzzonenplan;\n" +
                        "    END Beschriftung_Kommunale_SchutzplanungAssocAssoc;\n" +
                        "\n" +
                        "\n" +
                        "  END Kommunale_Schutzplanung;\n" +
                        "\n" +
                        "END AR_Schutzplanung_kommunal_20191101.\n" +
                        "\n",
                "2019-11-01",
                "fd9a88f87f73933e2291bea9328ca3af",
                false
        ));
        return iliFiles;
    }

}

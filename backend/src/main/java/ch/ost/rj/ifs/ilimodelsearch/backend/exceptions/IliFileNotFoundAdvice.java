package ch.ost.rj.ifs.ilimodelsearch.backend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class IliFileNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(IliFileNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String iliFileNotFoundHandler(IliFileNotFoundException ex) {
        return ex.getMessage();
    }
}

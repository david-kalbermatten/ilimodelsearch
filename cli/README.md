# IliModelSerach - CLI <!-- omit in toc -->

IliModelSearch is a tool for making text searches on Interlis files
on online data repositories (e.g. [this one](http://models.interlis.ch/)).
This makes it useful for locating geoinformation on a specific topic,
which can then be processed further by other tools.
This is the command line version.
An alternative tool is [ModelFinder](https://modelfinder.sogeo.services/).

- [How to run](#how-to-run)
  - [Requirements](#requirements)
  - [Usage](#usage)
    - [Basic](#basic)
    - [All Options](#all-options)
  - [Examples](#examples)
    - [Example 1](#example-1)
    - [Example 2](#example-2)
  - [Optional Steps](#optional-steps)
    - [Enable Umlaut (ANSI) Support on Windows](#enable-umlaut-ansi-support-on-windows)
- [How to contribute and run the project locally](#how-to-contribute-and-run-the-project-locally)
  - [Requirements](#requirements-1)
  - [Setup](#setup-1)
    - [Core](#core)
    - [Harvester](#harvester)
    - [CLI](#cli)

# How to run

## Requirements

- Terminal (CMD or PowerShell on Windows)
- [Java Runtime Version 11](https://docs.oracle.com/en/java/javase/11/install/overview-jdk-installation.html#GUID-8677A77F-231A-40F7-98B9-1FD0B48C346A)
- [ILIModelSearch CLI Jar File](https://gitlab.com/geometalab/ilimodelsearch/-/releases)

## Usage

### Basic

Open a terminal at the location of the jar file.
Execute the following command, replacing SEARCH_TERM with your search term:

```shell
java -jar ilimodelsearch-cli-0.5.jar -s "SEARCH_TERM" -l
```

### All Options

```cmd
Usage: <main class> [options]
  Options:
    -a, --alternateSearch
      Only searches the file names instead of the content
      Default: false
    -h, --help
      Displays this message
    -q, --quick
      Runs application without harvester
      Default: false
    -l, --showLineNumbers
      Shows line numbers in console output
      Default: false
    -f, -fuzziness
      Defines the degree of fuzziness to be used in search (between 0 and 2)
      Default: 1
    -n, -numberOfLines
      Defines how many lines of IliFile Content are to be displayed per Search 
      Result
      Default: 20
    -s, -search
      Term to be used for search
      Default: <empty string>
    -v, -verbose
      Level of verbosity
      Default: 2
```

## Examples

### Example 1

Update, search, with Line Numbers, numberOfLines set to 15
```shell
java -jar ilimodelsearch-cli-0.5.jar -s "fluss" -l -n 15
```

Output
```log
16:51:24 INFO  [main] HarvestRunner      : Started Harvesting Run!
16:53:12 INFO  [main] HarvestRunner      : Finished Harvesting Run!
16:53:13 INFO  [main] ConsoleApplication : There are currently: 1922 IliFiles in the repository!
16:53:13 INFO  [main] ConsoleApplication : There were 11 hits for the search term: "fluss"
16:53:13 INFO  [main] SearchIndexService : There were 11 hits for the search term: "fluss"
"SZ_Baugesuche_gewaessernah_V1": http://models.geo.sz.ch/AFG/SZ_Baugesuche_gewaessernah_V1.ili
01 |	INTERLIS 2.3;
02 |	!!==============================================================================
03 |	!!@ File                = "SZ_Baugesuche_gewaessernah_V1.ili";
04 |	!!@ Title               = "Baugesuche in oder an Gewässern";
05 |	!!@ shortDescription    = "Baugesuche, die eine Veränderung eines Stand- oder Fliessgewässer nach sich ziehen";
06 |	!!@ Issuer              = "https://www.sz.ch/geoinformation";
07 |	!!@ technicalContact    = "mailto:geoportal@sz.ch";
08 |	!!@ furtherInformation  = "https://www.sz.ch/afu";
09 |	!!@ Identifikator       = "- - -";
10 |	!!@ Themennummer        = "A169";
11 |	!!@ iliCompilerVersion  = "5.0.8-20200401";
12 |	!!------------------------------------------------------------------------------
13 |	!! Todo: - - -
14 |	!!------------------------------------------------------------------------------
15 |	!!      Version     | wer | was
"VECTOR200_PASS_V04_05": http://models.geo.admin.ch/Swisstopo/replaced/vector200_V04_05.ili
01 |	TRANSFER INTERLIS1;
02 |	
03 |	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
04 |	!!
05 |	!! Version:   4.4
06 |	!! Sprache:   Deutsch
07 |	!! Dateiname: vektor200.ili  
08 |	!! Datum:     22.07.2004
09 |	!! Status:    Definitiv 
10 |	!! History:   11.01.2007 ltflu Einführen der VEC200_ObjectOrigin 
11 |	!!                             EBM,ERM,EGM. Achtung auch icd angepasst 
12 |	!!            03.03.2008 ltflu VEC200_AIRPORT IAT und Hight ist OPTIONAL
13 |	!!                       ltknz 
14 |	!!						13.03.2008 ltlfu Bei den Railways Standseilbahn und 
15 |	!!                       ltknz Luftseilbahn eingeführt
"VECTOR200_PASS_V04_04": http://models.geo.admin.ch/Swisstopo/replaced/vector200_V04_04.ili
01 |	TRANSFER INTERLIS1;
02 |	
03 |	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
04 |	!!
05 |	!! Version:   4.4
06 |	!! Sprache:   Deutsch
07 |	!! Dateiname: vektor200.ili  
08 |	!! Datum:     22.07.2004
09 |	!! Status:    Definitiv 
10 |	!! History:   11.01.2007 ltflu Einführen der VEC200_ObjectOrigin 
11 |	!!                             EBM,ERM,EGM. Achtung auch icd angepasst 
12 |	!!            03.03.2008 ltflu VEC200_AIRPORT IAT und Hight ist OPTIONAL
13 |	!!                       ltknz 
14 |	!!						13.03.2008 ltlfu Bei den Railways Standseilbahn und 
15 |	!!                       ltknz Luftseilbahn eingeführt
...
```

### Example 2

No update, no line numbers, no lines, just search

```shell
java -jar ilimodelsearch-cli-0.5.jar -s "fluss" -q -n 0 
```

Output
```log
17:13:00 INFO  [main] ConsoleApplication : Skipping Harvester run!
17:13:01 INFO  [main] ConsoleApplication : There are currently: 1922 IliFiles in the repository!
17:13:02 INFO  [main] SearchIndexService : There were 11 hits for the search term: "fluss"
"SZ_Baugesuche_gewaessernah_V1": http://models.geo.sz.ch/AFG/SZ_Baugesuche_gewaessernah_V1.ili17:13:02 INFO  [main] SearchIndexService : "VECTOR200_PASS_V04_05": http://models.geo.admin.ch/Swisstopo/replaced/vector200_V04_05.ili
"VECTOR200_PASS_V04_04": http://models.geo.admin.ch/Swisstopo/replaced/vector200_V04_04.ili17:13:02 INFO  [main] SearchIndexService : "swissTLMRegio_ili2_LV95_V3": http://models.geo.admin.ch/Swisstopo/swissTLMRegio_ili2_LV95_V3.ili
"VECTOR200_PASS_V04_03": http://models.geo.admin.ch/Swisstopo/replaced/vector200_V04_03.ili17:13:02 INFO  [main] SearchIndexService : "swissTLMRegio_ili2_LV95_V2": http://models.geo.admin.ch/Swisstopo/replaced/swissTLMRegio_ili2_LV95_V2.ili
"VECTOR25_V01_07": http://models.geo.admin.ch/Swisstopo/replaced/vector25.ili
"VECTOR200_PASS_V04_02": http://models.geo.admin.ch/Swisstopo/replaced/vector200.ili
"VECTOR200_PASS_V05": http://models.geo.admin.ch/Swisstopo/replaced/vector200_V05.ili
"swissTLMRegio_ILI2_LV95_V1": http://models.geo.admin.ch/Swisstopo/replaced/swissTLMRegio_ILI2_LV95_V1.ili
"swissTLMRegio_ILI2_LV03_V1": http://models.geo.admin.ch/Swisstopo/replaced/swissTLMRegio_ILI2_LV03_V1.ili
```

## Optional Steps

### Enable Umlaut (ANSI) Support on Windows

For **PowerShell**:
```cmd
Set-ItemProperty HKCU:\Console VirtualTerminalLevel -Type DWORD 1
```
For **CMD**:
```cmd
reg add HKCU\Console /v VirtualTerminalLevel /t REG_DWORD /d 1
```

# How to contribute and run the project locally
This paragraph describes hot to setup the development environment(s).

## Requirements
- Java JDK Version 11
- IntelliJ
- Git

## Setup
In order for the seperate projects to work
you need to use `maven install`
to make the maven projects locally available.
__The order of installation matters!__

`git clone https://gitlab.com/geometalab/ilimodelsearch.git`

### Core
1. Open the `core` folder as a project in IntelliJ
2. Wait for IntelliJ to resolve all dependencies.
3. Open the maven side menu in IntelliJ
   and run the following commands by clicking them
   1. `clean`
   2. `install`

After sucessfully running this
you should be able to use the `core`-package in the other two projects:
`harvester`, `backend`

### Harvester
1. Open the `harvester` folder as a project in IntelliJ
2. Wait for IntelliJ to resolve all dependencies.
3. Open the maven side menu in IntelliJ
   and run the following commands by clicking them
   1. `clean`
   2. `install`

The harvester is setup to compare with the existing data set and update where needed.

### CLI
_The backend uses the H2-Memory DB._

1. Open the `cli` folder as a project in IntelliJ
2. Wait for IntelliJ to resolve all dependencies.
3. Navigate to the class called `Launcher`
4. Press the green launch button to run the cli application

When launching the first time,
the cli application will do a complete a full harvesting run
to build the local database.

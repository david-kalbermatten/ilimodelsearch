package ch.ost.rj.ifs.ilimodelsearch.cli;

import ch.ost.rj.ifs.ilimodelsearch.cli.service.ArgumentService;
import ch.ost.rj.ifs.ilimodelsearch.cli.repository.IliFileRepository;
import ch.ost.rj.ifs.ilimodelsearch.cli.repository.IliFileRepositoryConnector;
import ch.ost.rj.ifs.ilimodelsearch.cli.service.SearchIndexService;
import ch.ost.rj.ifs.ilimodelsearch.harvester.HarvestRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;

@Slf4j
@Component
@RequiredArgsConstructor
public class ConsoleApplication {
    private final ArgumentService argumentService;
    private final SearchIndexService searchIndexService;
    private final IliFileRepository repository;
    private final IliFileRepositoryConnector connector;

    @Value("${start.url}")
    private String startUrl;

    @Bean(name = "harvester")
    CommandLineRunner harvester() {
        return args -> {
            if (argumentService.quick) {
                log.info("Skipping Harvester run!");
            } else {
                try {
                    HarvestRunner harvestRunner = new HarvestRunner(startUrl, connector);
                    harvestRunner.run();
                } catch (MalformedURLException e) {
                    log.error("Start-URL was invalid: " + startUrl);
                }
            }
        };
    }

    @Bean(name = "search")
    @DependsOn({"harvester"})
    CommandLineRunner search() {
        return args -> {
            searchIndexService.addIliFiles(repository.findAll());
            log.info("There are currently: " + repository.count() + " IliFiles in the repository!");
            if (argumentService.searchTerm.isEmpty()) {
                log.error("Please provide a search term with -s {your search term}");
            } else {
                searchIndexService.search(argumentService.searchTerm);
                searchIndexService.print(argumentService.showLineNumbers, argumentService.numberOfLines);
            }
        };
    }
}
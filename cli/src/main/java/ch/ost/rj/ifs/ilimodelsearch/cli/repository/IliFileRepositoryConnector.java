package ch.ost.rj.ifs.ilimodelsearch.cli.repository;

import ch.ost.rj.ifs.ilimodelsearch.cli.exception.IliFileNotFoundException;
import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import ch.ost.rj.ifs.ilimodelsearch.harvester.repository.Repository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.NoSuchElementException;


@Service
@RequiredArgsConstructor
public class IliFileRepositoryConnector implements Repository {
    private final IliFileRepository repository;

    @Override
    public IliFile findById(String s) {
        if (s == null) s = "";
        final String id = s;
        try {
            return repository.findById(id).orElseThrow(() -> new IliFileNotFoundException(id));
        } catch (IliFileNotFoundException ex) {
            return null;
        }
    }

    @Override
    public void put(IliFile newIliFile) {
        String id = newIliFile.getId();
        repository.findById(id)
                .map(iliFileFromDB -> {
                    iliFileFromDB.setUrl(newIliFile.getUrl());
                    iliFileFromDB.setName(newIliFile.getName());
                    iliFileFromDB.setSchemaLanguage(newIliFile.getSchemaLanguage());
                    iliFileFromDB.setContent(newIliFile.getContent());
                    iliFileFromDB.setVersion(newIliFile.getVersion());
                    iliFileFromDB.setMd5(newIliFile.getMd5());
                    iliFileFromDB.setValidEncoding(newIliFile.isValidEncoding());
                    return repository.save(iliFileFromDB);
                })
                .orElseGet(() -> {
                    newIliFile.setId(id);
                    return repository.save(newIliFile);
                });
    }
}

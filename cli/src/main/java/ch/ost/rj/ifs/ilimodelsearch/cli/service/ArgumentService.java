package ch.ost.rj.ifs.ilimodelsearch.cli.service;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ArgumentService {
    @Parameter
    public List<String> parameters = new ArrayList<>();

    @Parameter(names = {"-s", "-search"}, description = "Term to be used for search")
    public String searchTerm = "";

    @Parameter(names = {"-f", "-fuzziness"}, description = "Defines the degree of fuzziness to be used in search (between 0 and 2)")
    public int fuzziness = 1;

    @Parameter(names = {"-q", "--quick"}, description = "Runs application without harvester")
    public boolean quick;

    @Parameter(names = {"-l", "--showLineNumbers"}, description = "Shows line numbers in console output")
    public boolean showLineNumbers;

    @Parameter(names = {"-a", "--alternateSearch"}, description = "Only searches the file names instead of the content")
    public boolean alternateSearch;

    @Parameter(names = {"-n", "-numberOfLines"}, description = "Defines how many lines of IliFile Content are to be displayed per Search Result")
    public int numberOfLines = 20;

    @Parameter(names = {"-v", "-verbose"}, description = "Level of verbosity")
    public int verbose = 2;

    @Parameter(names = {"-h", "--help"}, help = true, description = "Displays this message")
    public boolean help;


    public void parseArgs(String[] args) {
        JCommander jcm = JCommander.newBuilder()
                .addObject(this)
                .build();
        jcm.parse(args);
        if (help) {
            jcm.usage();
            System.exit(0);
        }
    }
}

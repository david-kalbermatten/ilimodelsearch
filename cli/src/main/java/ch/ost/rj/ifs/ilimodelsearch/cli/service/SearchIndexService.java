package ch.ost.rj.ifs.ilimodelsearch.cli.service;

import ch.ost.rj.ifs.ilimodelsearch.cli.helper.Unescaper;
import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.*;
import org.apache.lucene.index.memory.MemoryIndex;
import org.apache.lucene.queryparser.simple.SimpleQueryParser;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.WildcardQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Data
@Service
public class SearchIndexService {
    private final Map<IliFile, MemoryIndex> dictionary = new HashMap<>();
    private final Analyzer analyzer = new StandardAnalyzer();
    private final ArgumentService args;

    private List<IliFile> searchResults = new ArrayList<>();
    private String currentSearchTerm;

    public void addIliFiles(List<IliFile> iliFiles) {
        iliFiles.forEach(this::addIliFile);
    }

    public void addIliFile(IliFile iliFile) {
        MemoryIndex index = new MemoryIndex();
        index.addField("id", iliFile.getId(), analyzer);
        index.addField("name", iliFile.getName(), analyzer);
        index.addField("content", iliFile.getContent(), analyzer);
        dictionary.put(iliFile, index);
    }

    public void search(String query) {
        currentSearchTerm = query;
        searchResults.clear();
        Term term;
        if(args.alternateSearch) {
            term = new Term("name", query);
        } else {
            term = new Term("content", query);
        }
        dictionary.forEach((iliFile, memoryIndex) -> {
            float fuzzyScore = memoryIndex.search(new FuzzyQuery(term, args.fuzziness));
            float hitScore = memoryIndex.search(new WildcardQuery(term));
            if (hitScore > 0.0 || fuzzyScore > 0.0) searchResults.add(iliFile);
        });
    }

    public void print(boolean lineNumbers, int numberOfLines) {
        log.info("There were " + searchResults.size() + " hits for the search term: " + "\"" + currentSearchTerm + "\"");
        searchResults.forEach(iliFile -> {
            List<String> firstNLines = Arrays.stream(iliFile.getContent().split("\n"))
                    .limit(numberOfLines)
                    .collect(Collectors.toList());
            List<String> shortContent = new ArrayList<>();

            for (int i = 0; i < firstNLines.size(); i++) {
                String s = firstNLines.get(i);
                String output = "";
                if(lineNumbers) {
                    output += String.format("%02d", i + 1) + " ";
                }
                output += "|\t" + s;
                shortContent.add(output);
            }

            String header = "\"" + iliFile.getName() + "\":" + iliFile.getUrl();
            String content = String.join("\n", shortContent);
            if (content.isBlank()) {
                System.out.println(header);
            } else {
                System.out.println(header + "\n" + content);
            }
        });
    }
}

package ch.ost.rj.ifs.ilimodelsearch.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.*;

import javax.persistence.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

@Data
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString()
public class IliFile {

    @Id
    @EqualsAndHashCode.Include
    protected String id;

    protected URL url;

    protected String name;
    protected String schemaLanguage;

    @ToString.Exclude
    @Column(columnDefinition = "TEXT")
    protected String content;
    protected String version;
    protected String md5;
    protected boolean validEncoding;

    public IliFile(URL url, String name, String schemaLanguage, String content, String version, String md5, boolean validEncoding) {
        this.id = name + version;
        this.url = url;
        this.name = name;
        this.schemaLanguage = schemaLanguage;
        this.content = content;
        this.version = version;
        this.md5 = md5;
        this.validEncoding = validEncoding;
    }

    public IliFile(String json) throws JsonProcessingException {
        IliFile iliFile = fromJson(json);
        this.id = iliFile.getId();
        this.url = iliFile.getUrl();
        this.name = iliFile.getName();
        this.schemaLanguage = iliFile.getSchemaLanguage();
        this.content = iliFile.getContent();
        this.version = iliFile.getVersion();
        this.md5 = iliFile.getMd5();
        this.validEncoding = iliFile.isValidEncoding();
    }

    /**
     * This function checks a string for the invalid character "�"
     * @param content The iliFile Content
     * @return true if a bad character is found
     */
    public static boolean checkForBadEncoding(String content) {
        return content.contains("�");
    }

    public String toJson() throws JsonProcessingException {
        return IliFileSerializer.toJson(this);
    }

    private static IliFile fromJson(String json) throws JsonProcessingException {
        return IliFileDeserializer.toIliFile(json);
    }

    @Override
    public IliFile clone() throws CloneNotSupportedException {
        try {
            return new IliFile(
                    this.getId(),
                    new URL(this.url.toString()),
                    this.getName(),
                    this.getSchemaLanguage(),
                    this.getContent(),
                    this.getVersion(),
                    this.getMd5(),
                    this.isValidEncoding()
            );
        } catch (MalformedURLException e) {
            return (IliFile) super.clone();
        }
    }
}

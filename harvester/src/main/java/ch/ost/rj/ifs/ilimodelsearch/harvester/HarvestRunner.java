package ch.ost.rj.ifs.ilimodelsearch.harvester;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import ch.ost.rj.ifs.ilimodelsearch.harvester.model.DownloadableIliFile;
import ch.ost.rj.ifs.ilimodelsearch.harvester.model.xml.IliModelsXml;
import ch.ost.rj.ifs.ilimodelsearch.harvester.repository.Repository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class HarvestRunner {
    private final Harvester harvester;
    private final Repository repository;

    public HarvestRunner(@NonNull String startURL, @NonNull Repository repository) throws MalformedURLException {
        this.harvester = new Harvester(startURL);
        this.repository = repository;
    }

    public void run() {
        log.info("Started Harvesting Run!");
        List<IliModelsXml> iliModelsXmlList = harvester.getIliModels();
        List<IliFile> allIliFiles = new ArrayList<>();
        iliModelsXmlList.forEach(iliModelsXml -> allIliFiles.addAll(iliModelsXml.getIliFiles()));

        List<IliFile> newIliFiles = getOnlyNewIliFiles(allIliFiles);
        addIliFilesToRepository(newIliFiles);
        log.info("Finished Harvesting Run!");
    }

    private List<IliFile> getOnlyNewIliFiles(List<IliFile> iliFiles) {
        List<IliFile> iliFilesToPost = new ArrayList<>();
        iliFiles.forEach(iliFile -> {
            DownloadableIliFile downloadableIliFile = (DownloadableIliFile) iliFile;
            IliFile iliFIleFromBackend = repository.findById(iliFile.getId());

            if (iliFIleFromBackend != null) {
                if (iliFile.getMd5() == null || iliFile.getMd5().isEmpty() || iliFile.getMd5().isBlank()) {
                    log.debug("Duplicate Check \"MD5 is missing\": " + iliFile.getUrl());
                    try {
                        downloadableIliFile.fetchContent();
                        iliFilesToPost.add(downloadableIliFile);
                    } catch (IOException e) {
                        log.warn("Couldn't download IliFile Content: " + iliFile.getUrl());
                    }
                } else if (!iliFile.getMd5().equals(iliFIleFromBackend.getMd5())) {
                    log.debug("Duplicate Check \"newer version\": " + iliFile.getUrl());
                    try {
                        downloadableIliFile.fetchContent();
                        iliFilesToPost.add(downloadableIliFile);
                    } catch (IOException e) {
                        log.warn("Couldn't download IliFile Content: " + iliFile.getUrl());
                    }
                } else {
                    log.debug("This IliFile was skipped, because it already exists in DB: ");
                    log.debug(iliFIleFromBackend.toString());
                }
            } else {
                try {
                    log.debug("Duplicate Check \"new file\": " + iliFile.getUrl());
                    downloadableIliFile.fetchContent();
                    iliFilesToPost.add(downloadableIliFile);
                } catch (IOException e) {
                    log.warn("Couldn't download IliFile Content: " + iliFile.getUrl());
                }
            }
        });
        return iliFilesToPost;
    }

    public void addIliFilesToRepository(List<IliFile> iliFiles) {
        iliFiles.forEach(iliFile -> {
            try {
                repository.put(iliFile.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            log.debug("Successfully pushed Entry to Backend: " + iliFile.getUrl());
        });
    }
}

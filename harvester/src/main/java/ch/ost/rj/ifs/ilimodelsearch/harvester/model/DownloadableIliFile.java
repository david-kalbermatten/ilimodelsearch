package ch.ost.rj.ifs.ilimodelsearch.harvester.model;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import ch.ost.rj.ifs.ilimodelsearch.harvester.helper.CharsetDetector;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Slf4j
public class DownloadableIliFile extends IliFile {
    public DownloadableIliFile(String id, URL url, String name, String schemaLanguage, String version, String md5) {
        super.id = id;
        super.url = url;
        super.name = name;
        super.schemaLanguage = schemaLanguage;
        super.version = version;
        super.md5 = md5;
    }

    public DownloadableIliFile(URL url, String name, String schemaLanguage, String version, String md5) {
        this(name + version, url, name, schemaLanguage, version, md5);
    }

    public void fetchContent() throws IOException {
        InputStream in = url.openStream();
        byte[] iliFileBytes = in.readAllBytes();
        in.close();

        String charset = new CharsetDetector().detectCharset(new ByteArrayInputStream(iliFileBytes));
        String iliFileContent;

        if (charset.equals(CharsetDetector.NO_MATCH) || charset.equals("UTF-8")) {
            iliFileContent = new String(iliFileBytes, StandardCharsets.UTF_8);
        } else {
            Charset charsetEncoding = Charset.forName(charset);
            iliFileContent = new String(iliFileBytes, charsetEncoding);
            if (iliFileContent.contains("�")) {
                charsetEncoding = Charset.forName("windows-1252");
                iliFileContent = new String(iliFileBytes, charsetEncoding);
            }
        }

        log.debug("Successfully downloaded: " + url);

        super.content = iliFileContent;
        super.validEncoding = !IliFile.checkForBadEncoding(iliFileContent);
    }
}

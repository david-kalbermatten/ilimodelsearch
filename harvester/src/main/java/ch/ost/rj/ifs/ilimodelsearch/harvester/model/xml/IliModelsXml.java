package ch.ost.rj.ifs.ilimodelsearch.harvester.model.xml;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;
import ch.ost.rj.ifs.ilimodelsearch.harvester.model.DownloadableIliFile;
import lombok.extern.slf4j.Slf4j;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.ElementFilter;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class IliModelsXml extends IliXml {

    public IliModelsXml(URL url) throws IOException, JDOMException {
        super(url);
    }

    public List<IliFile> getIliFiles() {
        List<IliFile> iliFiles = new ArrayList<>();
        List<Element> metaDataList = getModelMetaDataList();
        for (Element metaData : metaDataList) {
            try {
                IliFile tmpIliFile = generateIliFile(metaData);
                iliFiles.add(tmpIliFile);
            } catch (IOException e) {
                log.error("The following ili-file url was unreachable: " + e.getMessage());
                log.error("Corresponding ilimodels.xml url: " + this.url.toString());

                try {
                    String path = "C:/tmp/list_of_know_unreachable_urls.txt";
                    File file = new File(path);
                    if (!file.exists()) file.createNewFile();

                    FileWriter fw = new FileWriter(file, true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    PrintWriter pW = new PrintWriter(bw);

                    pW.print("The following ili-file url was unreachable: ");
                    pW.println(e.getMessage());
                    pW.print("Corresponding ilimodels.xml url: ");
                    pW.println(this.url.toString());
                    pW.close();
                } catch (IOException fileNotFoundException) {
                    log.error("Couldn't write file: " + fileNotFoundException);
                }
            }
        }
        return iliFiles;
    }

    private IliFile generateIliFile(Element element) throws IOException {
        URL url = getAbsoluteURL(element.getChildText("File", element.getNamespace()));
        String name = element.getChildText("Name", element.getNamespace());
        String schemaLanguage = element.getChildText("SchemaLanguage", element.getNamespace());
        String md5 = element.getChildText("md5", element.getNamespace());
        String version = element.getChildText("Version", element.getNamespace());

        return new DownloadableIliFile(url, name, schemaLanguage, version, md5);
    }

    private List<Element> getModelMetaDataList() {
        List<Element> modelMetaDataList = new ArrayList<>();
        for (Element file : document.getRootElement().getDescendants(new ElementFilter("File"))) {
            modelMetaDataList.add(file.getParentElement());
        }
        return modelMetaDataList;
    }

    private URL getAbsoluteURL(String relativeUrl) {
        try {
            return new URL(this.url.toString().replace("/ilimodels.xml", "/".concat(relativeUrl)));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.out.println("Malformed URL!");
        }
        return null;
    }
}

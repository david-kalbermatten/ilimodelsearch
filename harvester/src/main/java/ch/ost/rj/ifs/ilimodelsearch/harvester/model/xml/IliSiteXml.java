package ch.ost.rj.ifs.ilimodelsearch.harvester.model.xml;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.ElementFilter;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class IliSiteXml extends IliXml {
    public IliSiteXml(URL url) throws IOException, JDOMException {
        super(url);
    }

    public List<String> getSubsidiarySites() {
        List<String> subsidiarySites = new ArrayList<>();
        document.getRootElement().getDescendants(new ElementFilter("subsidiarySite")).forEach(subsidiarySiteRepo -> {
            for (Element value : subsidiarySiteRepo.getDescendants(new ElementFilter("value"))) {
                subsidiarySites.add(value.getValue());
            }
        });

        return subsidiarySites;
    }
}

package ch.ost.rj.ifs.ilimodelsearch.harvester.repository;

import ch.ost.rj.ifs.ilimodelsearch.core.model.IliFile;

public interface Repository {
    IliFile findById(String id);

    void put(IliFile iliFile);
}
